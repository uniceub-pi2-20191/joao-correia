
import {AppRegistry} from 'react-native';
import Login from './src/components/Login/Login';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Login);
